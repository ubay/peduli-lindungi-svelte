
/**
 * @roxi/routify 2.18.4
 * File generated Sun Jan 23 2022 16:06:07 GMT+0700 (Western Indonesia Time)
 */

export const __version = "2.18.4"
export const __timestamp = "2022-01-23T09:06:07.361Z"

//buildRoutes
import { buildClientTree } from "@roxi/routify/runtime/buildRoutes"

//imports


//options
export const options = {}

//tree
export const _tree = {
  "name": "_layout",
  "filepath": "/_layout.svelte",
  "root": true,
  "ownMeta": {},
  "absolutePath": "E:/CODE/svelte/peduli-lindungi-clone/src/pages/_layout.svelte",
  "children": [
    {
      "isFile": true,
      "isDir": false,
      "file": "_fallback.svelte",
      "filepath": "/_fallback.svelte",
      "name": "_fallback",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "E:/CODE/svelte/peduli-lindungi-clone/src/pages/_fallback.svelte",
      "importPath": "../src/pages/_fallback.svelte",
      "isLayout": false,
      "isReset": false,
      "isIndex": false,
      "isFallback": true,
      "isPage": false,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": false,
        "prerender": true
      },
      "path": "/_fallback",
      "id": "__fallback",
      "component": () => import('../src/pages/_fallback.svelte').then(m => m.default)
    },
    {
      "isFile": true,
      "isDir": false,
      "file": "index.svelte",
      "filepath": "/index.svelte",
      "name": "index",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "E:/CODE/svelte/peduli-lindungi-clone/src/pages/index.svelte",
      "importPath": "../src/pages/index.svelte",
      "isLayout": false,
      "isReset": false,
      "isIndex": true,
      "isFallback": false,
      "isPage": true,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": false,
        "prerender": true
      },
      "path": "/index",
      "id": "_index",
      "component": () => import('../src/pages/index.svelte').then(m => m.default)
    },
    {
      "isFile": true,
      "isDir": false,
      "file": "tentang.svelte",
      "filepath": "/tentang.svelte",
      "name": "tentang",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "E:/CODE/svelte/peduli-lindungi-clone/src/pages/tentang.svelte",
      "importPath": "../src/pages/tentang.svelte",
      "isLayout": false,
      "isReset": false,
      "isIndex": false,
      "isFallback": false,
      "isPage": true,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": false,
        "prerender": true
      },
      "path": "/tentang",
      "id": "_tentang",
      "component": () => import('../src/pages/tentang.svelte').then(m => m.default)
    },
    {
      "isFile": true,
      "isDir": false,
      "file": "unduh.svelte",
      "filepath": "/unduh.svelte",
      "name": "unduh",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "E:/CODE/svelte/peduli-lindungi-clone/src/pages/unduh.svelte",
      "importPath": "../src/pages/unduh.svelte",
      "isLayout": false,
      "isReset": false,
      "isIndex": false,
      "isFallback": false,
      "isPage": true,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": false,
        "prerender": true
      },
      "path": "/unduh",
      "id": "_unduh",
      "component": () => import('../src/pages/unduh.svelte').then(m => m.default)
    }
  ],
  "isLayout": true,
  "isReset": false,
  "isIndex": false,
  "isFallback": false,
  "isPage": false,
  "isFile": true,
  "file": "_layout.svelte",
  "ext": "svelte",
  "badExt": false,
  "importPath": "../src/pages/_layout.svelte",
  "meta": {
    "recursive": true,
    "preload": false,
    "prerender": true
  },
  "path": "/",
  "id": "__layout",
  "component": () => import('../src/pages/_layout.svelte').then(m => m.default)
}


export const {tree, routes} = buildClientTree(_tree)

